import json

class PubmedAbstract(object):
	def __init__(self):
		pass

	def __str__(self):
		return self.id + " " + self.title
	def toJSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

