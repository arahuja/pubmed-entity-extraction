#!/usr/bin/env python

import zipimport

importer = zipimport.zipimporter('nltkandyaml.mod')
yaml = importer.load_module('yaml')
nltk = importer.load_module('nltk')
import json
import sys

from nltk.tag.stanford import NERTagger

GENE_NAMES = set(line.strip() for line in open('omim_gene.txt'))

def get_entities(st, text):
	try:
		tagged = st.tag(text.split())
	except:
		return ""
        curr_phrase = ""
        entities = []
        for phrase, tag in tagged:
		if tag != "O" and tag != "TAG":
			curr_phrase += phrase + " "
		elif curr_phrase:
			entities.append("-".join(curr_phrase.strip().split()))
			curr_phrase = ""
	return " ".join(entities)

def get_genes(text):
	return ",".join(GENE_NAMES.intersection(set(text.split())))
	
	

def main():
	gene_st = NERTagger('genetag-model.ser.gz', 'stanford-ner.mod')
	disease_st = NERTagger('diseasetag-model.ser.gz', 'stanford-ner.mod')
	for line in sys.stdin:
		abstract = json.loads(line)
		line = abstract['title'] + " " + abstract['abstract']
		#print line + "\t" + get_entities(line) + "\t" + get_genes(line)
		#print get_genes(line) + "\t" + get_entities(gene_st, line) + "\t" + get_entities(disease_st, line) + "\t"+ line
		abstract['gene1'] = get_genes(line)
		abstract['gene2'] = get_entities(gene_st, line)
		abstract['diseases'] = get_entities(disease_st, line)
		print json.dumps(abstract)
	
if __name__=='__main__':
	main()
