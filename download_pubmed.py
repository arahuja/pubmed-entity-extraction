import logging
import urllib2
import urllib
import sys
import datetime
import argparse
from lxml import etree
import json
from pubmed_abstract import PubmedAbstract

""" 
Download data for a given day
python download_pubmed.py --year 2013 --month 10 --day 10
"""

def retrieve_text_element(el, path):
	path_el = el.find(path)
	if path_el is not None:
		return path_el.text
	else:
		return ""

PUBMED_SEARCH_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?"
PUBMED_SEARCH_PARMS = \
	{ "db" : "pubmed", \
	  "retmax" : 100000}

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Download records from PubMed')
	parser.add_argument('--year', '-y', dest="year", type=int)
	parser.add_argument('--month', '-m', dest="month", type=int)
	parser.add_argument('--day', '-d', dest="day", type=int)

	args = parser.parse_args()
	
	LOG_FILENAME = "LOG." + sys.argv[0].split(".")[0] + ".log"	
	logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)

	download_date = datetime.date(args.year, args.month, args.day).isoformat()
	PUBMED_SEARCH_PARMS['term'] = datetime.date(args.year, args.month, args.day).isoformat()
	logging.info("Downloading PubMed Article IDS for: " + download_date)
	out = open(download_date+".pubmed", 'wb')
	
	query = urllib.urlencode(PUBMED_SEARCH_PARMS)

	response = urllib2.urlopen(PUBMED_SEARCH_URL+query).read()
	parsed_response = etree.XML(response)
	ids = parsed_response.xpath('/eSearchResult/IdList/Id//text()')
	id_query_string = ",".join(ids[:899])
	#print "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&rettype=abstract&id=" + id_query_string
	abstract_response = urllib2.urlopen("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&rettype=abstract&id=" + id_query_string).read()
	parsed_abstracts  = etree.XML(abstract_response)
	articles = parsed_abstracts.xpath("/PubmedArticleSet/PubmedArticle")
	for article in articles:
		pa = PubmedAbstract()	
		pa.pmid = retrieve_text_element(article, "MedlineCitation/PMID")
		pa.title = retrieve_text_element(article, "MedlineCitation/Article/ArticleTitle")
		pa.abstract = retrieve_text_element(article,"MedlineCitation/Article/Abstract/AbstractText")
		pa.date = "-".join([retrieve_text_element(article, "MedlineCitation/DateCreated/Year"), retrieve_text_element(article, "MedlineCitation/DateCreated/Month"), retrieve_text_element(article, "MedlineCitation/DateCreated/Day")])
		out.write(pa.toJSON() + "\n")
